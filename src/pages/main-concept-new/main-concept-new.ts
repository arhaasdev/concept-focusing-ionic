import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ConceptService } from '../../providers/concept-service';
import { ConceptListPage } from '../concept-list/concept-list';

@Component({
  selector: 'page-main-concept-new',
  templateUrl: 'main-concept-new.html'
})

export class MainConceptNewPage {
  name = "";
  definition = "";
  relatedConcept1 = "";
  relation1Description = "";
  relatedConcept2 = "";
  relation2Description = "";
  relatedConcept3 = "";
  relation3Description = "";
  relatedConcept4 = "";
  relation4Description = "";

  constructor(
    public navCtrl: NavController,
    public conceptService: ConceptService
  ) {}

  save() {
    this.conceptService.createConcept({
      name: this.name,
      definition: this.definition,
      relatedConcept1: this.relatedConcept1,
      relation1Description: this.relation1Description,
      relatedConcep2t: this.relatedConcept2,
      relation2Description: this.relation2Description,
      relatedConcept3: this.relatedConcept3,
      relation3Description: this.relation3Description,
      relatedConcept4: this.relatedConcept4,
      relation4Description: this.relation4Description,
    })
    .then(() => {
      this.navCtrl.setRoot(ConceptListPage);
    });
  }

  cancel() {
    this.navCtrl.setRoot(ConceptListPage);
  }

  ionViewDidLoad() {
    console.log('Hello MainConceptNewPage Page');
  }
}
