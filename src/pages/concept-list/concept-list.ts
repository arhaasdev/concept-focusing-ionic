import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SortOptionsPage } from '../sort-options/sort-options';
import { MainConceptDetailsPage } from '../main-concept-details/main-concept-details';
import { MainConceptEditPage } from '../main-concept-edit/main-concept-edit';
import { ConceptService } from '../../providers/concept-service';

@Component({
    selector: 'concept-list-page',
    templateUrl: 'concept-list.html'
})
export class ConceptListPage {
    selectedItem: any;
    mainConcepts: any;

    constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public conceptService: ConceptService
    ) {
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        this.loadConcepts();
    }

    loadConcepts() {
        return this.conceptService.getConcepts()
            .then((data) => {
              this.mainConcepts = data;
              console.log(this.mainConcepts);
            })
            .catch(error => {                                   // Login sequence failed
              console.log('ConceptListPage loadConcepts() : error', error);
              console.log('');
              console.log('ConceptListPage loadConcepts() : error', ' status =', error.status);
              console.log('ConceptListPage loadConcepts() : error', 'human readable message: ', JSON.parse(error._body).err.message);
            });

            // TODO: Trigger the display of appropriate message using err body
            // properties: `status' and `name'
            // TODO: Trigger the loading of appropriate page
    }

    //showConcepts() ???

  openDetailsPage(event, item) {
    this.navCtrl.push(MainConceptDetailsPage, {
      item: item
    });
  }

  openMainConceptEditPage(event, item) {
    this.navCtrl.push(MainConceptEditPage, {
      item: item
    });
  }

  deleteConcept(event, item) {
      this.conceptService.deleteConcept(item._id)
      .then(() => {
        this.navCtrl.setRoot(ConceptListPage);
      });
  }

  openSortOptions() {
      this.navCtrl.push(SortOptionsPage);
  };

  ionViewDidLoad() {
    console.log('at ConceptListPage');
  }
}
