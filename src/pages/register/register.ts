import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthenticationService } from '../../providers/authentication-service';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  private firstname: string;
  private lastname: string;
  private username: string;
  private password: string;

  constructor(
      public navCtrl: NavController,
      private authenticationService: AuthenticationService) {
  }

  cancel() {
      this.navCtrl.pop();
  }

  register() {
      var response = this.authenticationService.register({
            firstname: this.firstname,
            lastname: this.lastname,
            username: this.username,
            password: this.password});
      console.log(response);
  }

  onSuccess(response: any) {
      console.log("registration success");


  }

  onFailure(response: any) {
      console.log("registration failure");


  }

  ionViewDidLoad() {
    console.log('Hello RegisterPage Page');
  }
}
