import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the SortOptions page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'sort-options.html'
})

export class SortOptionsPage {
  selectedItem: any;
  icons: string[];
  // Author:RAH todo remove mainConcepts below
  mainConcepts: Array<{title: string, note: string, icon: string}>;
  sortMethod: string;
  ordering: string;

  constructor(public navCtrl: NavController) {
    this.sortMethod = "alphanumeric";
    this.ordering = "descending";
  }

  ionViewDidLoad() {
    console.log('Hello SortOptionsPage Page');
  }

// Author:RAH doc: <ion-list radio-group ...> sets sortMethod with the corresponding
// radio button value
  selectSortMethod() {
    console.log("sortMethod == ", this.sortMethod);
  }

  selectOrdering() {
    console.log("ordering == ", this.ordering);
  }
}
