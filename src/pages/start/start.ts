import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthenticationService } from '../../providers/authentication-service';
import { RegisterPage } from '../register/register';
import { MyApp } from '../../app/app.component';
import { ConceptListPage } from '../concept-list/concept-list';

@Component({
  selector: 'page-start',
  templateUrl: 'start.html'
})

export class StartPage {
  private username: string;
  private password: string;


  constructor(
      public navCtrl: NavController,
      private authenticationService: AuthenticationService
  ) {}

  login() {
      this.authenticationService.logout()
      .then(() => {                                       // Asynch: assure current user is logged out
        return this.authenticationService.login(this.username, this.password)
      })
      .then(() => {
        console.log('login(): success');
      })
      .then(() => {                                       // Asynch: login was successful
          this.openConceptListPage();
      })
      .catch(error => {                                   // Login failed
        console.log('login(): error', error);
      });

      // .catch todo: display appropriate message using err body
      //                properties: status and name
      //              laod appropriate page

  }

  openRegisterPage() {
      this.navCtrl.push(RegisterPage);
  };

  openConceptListPage() {
      this.navCtrl.setRoot(ConceptListPage);
  }

  ionViewDidLoad() {
    console.log('Hello StartPage Page');
  }
}
