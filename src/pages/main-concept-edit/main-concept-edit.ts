import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ConceptListPage } from '../concept-list/concept-list';
import { ConceptService } from '../../providers/concept-service';

@Component({
  selector: 'page-main-concept-edit',
  templateUrl: 'main-concept-edit.html'
})
export class MainConceptEditPage {
  selectedItem: any;
  name: string;
  definition: string;
  relatedConcept1: string;
  relation1Description: string;
  relatedConcept2: string;
  relation2Description: string;
  relatedConcept3: string;
  relation3Description: string;
  relatedConcept4: string;
  relation4Description: string;

  constructor(public navCtrl: NavController,
  public navParams: NavParams,
  public conceptService: ConceptService
  ) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');
    this.name = this.selectedItem.name;
    this.definition = this.selectedItem.definition;
    this.relatedConcept1 = this.selectedItem.relatedConcept1;
    this.relation1Description = this.selectedItem.relation1Description;
    this.relatedConcept2 = this.selectedItem.relatedConcept2;
    this.relation2Description = this.selectedItem.relation2Description;
    this.relatedConcept3 = this.selectedItem.relatedConcept3;
    this.relation3Description = this.selectedItem.relation3Description;
    this.relatedConcept4 = this.selectedItem.relatedConcept4;
    this.relation4Description = this.selectedItem.relation4Description;

  }

  save() {
    this.conceptService.updateConcept({
      name: this.name,
      definition: this.definition,
      relatedConcept1: this.relatedConcept1,
      relation1Description: this.relation1Description,
      relatedConcept2: this.relatedConcept2,
      relation2Description: this.relation2Description,
      relatedConcept3: this.relatedConcept3,
      relation3Description: this.relation3Description,
      relatedConcept4: this.relatedConcept4,
      relation4Description: this.relation4Description,
    }, this.selectedItem._id)
    .then(() => {
      this.navCtrl.setRoot(ConceptListPage);
    });
  }

  cancel() {
    this.navCtrl.setRoot(ConceptListPage);
  }

  ionViewDidLoad() {
    console.log('Hello MainConceptEditPage Page');
  }
}
