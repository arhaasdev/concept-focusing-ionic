import { ErrorHandler } from '@angular/core';

export class AppErrorHandler implements ErrorHandler {
    // TODO: Determine which if any class variables need to be put here

    handleError(error) {
        // a human-readable message is available to display
        if ( error.message ) {
            console.log('An explainable error occured: ', error);
            this.showAlert(error.message);
        }
        else {
            console.log('An error occurred: ', error);
            // TODO: Implment calls for additional processing if needed
        }
    }

    // display human-readable message to the user in an alert dialog
    showAlert(error) {
        // TODO: Not yet implemented
    }
}
