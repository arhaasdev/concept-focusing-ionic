import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
//import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthenticationService {
  private authenticated: boolean;
  private registerHeaders = new Headers({'Content-Type': 'application/json'});
  private loginHeaders = new Headers({'Content-Type': 'application/json'});
  private registerUrl = 'http://conceptfocuscards.com/users/register';
  private loginUrl = 'http://conceptfocuscards.com/users/login';
  private logoutUrl = 'http://conceptfocuscards.com/users/logout';

  constructor(
      public http: Http,
      private storage: Storage) {
    this.authenticated = false;
  }

  register(registerData: any): Promise<any> {
      return this.http.post(this.registerUrl, JSON.stringify(registerData),
            {headers: this.registerHeaders})
        .toPromise()
        .then(res => res.json().data)
        .catch(this.handleError);
  }

  login(username: string, password: string): Promise<any> {
      let body = JSON.stringify({username: username, password: password});
      return this.http.post(this.loginUrl, body, {headers: this.loginHeaders})      // Asynchronous
        .toPromise()
        .then(data => {
          let userData = data.json();
          if (userData && userData.token) {                 // login successful if there's a jwt token in the response
            return this.storage.set('userToken', JSON.stringify(userData.token))
          }
        })
        .catch(error => {
            console.log('authentication-service.ts login: ', error);
       });
    }

  logout(): Promise<any>  {
    return this.storage.remove ('userToken')
      .then( () => {                                // Asynchronous -- remove current user
      console.log('logout():  removed any token');
    });
  }

  // MOVE THIS to separte service
  getAll(): Promise<any> {
  return this.jwt()                                                            // Asynchronous -- retrieve token, build headers
  .then((jwtResult) => {                                                 //  note: token identifies the user to the server
    console.log('getAll(): http jwtResult ', jwtResult );
    return this.http.get('http://conceptfocuscards.com/cards', jwtResult).map((response: Response) => response.json())
    .subscribe(
      (data) => {
        console.log('getAll(): success ', data);  // RAH diagnostic
       },
      (error) => {
        console.log('agetAll(): error', error);     // RAH diagnostic
      });
    })
}

public jwt(): Promise<any> {
return this.storage.get('userToken')                                    // Asynchronous
  .then( (tok) => {
    console.log ('jwt(): User token retrieved ', tok);
    let userToken = JSON.parse(tok);                                    // remove quotes from string
    console.log ('jwt(): User Token parsed ', userToken);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json ');
    headers.append('x-access-token', userToken);                                           // arhaasdev / RAH
    return new RequestOptions({ headers: headers });
  })
  .catch(error => {
    return console.error(error)
  });
 }

  public handleError(error: any): Promise<any> {
      console.error('An error occurred', error); // for demo purposes only
      return Promise.reject(error.message || error);
  }

  isAuthenticated() {
    return this.isAuthenticated;
  }
}
