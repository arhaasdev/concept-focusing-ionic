import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AuthenticationService } from './authentication-service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class ConceptService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService
    ) {}

    // asynchronously fetch all of the user's main concepts
    getConcepts(): Promise<any> {
        return this.authenticationService.jwt()                             // Asynch -- retrieve token, build headers
            .then((jwtResult) => {                                              //  note: token identifies the user to the server
                return this.http.get('http://conceptfocuscards.com/cards', jwtResult)
                .map((response: Response) => response.json())
                .toPromise()
                .then(data => {return data;})
        .catch(error => {
          return this.authenticationService.handleError(error);
        });
      })
    }

    // asynchronously create a main concept
    createConcept(conceptData): Promise<any> {
        return this.authenticationService.jwt()                             // Asynch -- retrieve token, build headers
            .then((jwtResult) => {                                              //  note: token identifies the user to the server
                return this.http.post('http://conceptfocuscards.com/cards', conceptData, jwtResult)
                .map((response: Response) => response.json())
                .toPromise()
                .then(data => {return data;})
        .catch(error => {
          return this.authenticationService.handleError(error);
        });
      })
    }

    // asynchronously update a main concept
    updateConcept(conceptData, id): Promise<any> {
        return this.authenticationService.jwt()                             // Asynch -- retrieve token, build headers
            .then((jwtResult) => {                                              //  note: token identifies the user to the server
                return this.http.put('http://conceptfocuscards.com/cards/' + id, conceptData, jwtResult)
                .map((response: Response) => response.json())
                .toPromise()
                .then(data => {return data;})
        .catch(error => {
          return this.authenticationService.handleError(error);
        });
      })
    }

    // asynchronously delete a main concept
    deleteConcept(id): Promise<any> {
        return this.authenticationService.jwt()                             // Asynch -- retrieve token, build headers
            .then((jwtResult) => {                                              //  note: token identifies the user to the server
                return this.http.delete('http://conceptfocuscards.com/cards/' + id, jwtResult)
                .map((response: Response) => response.json())
                .toPromise()
                .then(data => {return data;})
        .catch(error => {
          return this.authenticationService.handleError(error);
        });
      })
    }
}

