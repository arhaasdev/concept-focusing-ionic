import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { StartPage } from '../pages/start/start';
import { RegisterPage } from '../pages/register/register';
import { ConceptListPage } from '../pages/concept-list/concept-list';
import { MainConceptDetailsPage } from '../pages/main-concept-details/main-concept-details';
import { MainConceptEditPage } from '../pages/main-concept-edit/main-concept-edit';
import { MainConceptNewPage } from '../pages/main-concept-new/main-concept-new';
import { SortOptionsPage } from '../pages/sort-options/sort-options';
import { AuthenticationService } from '../providers/authentication-service';
import { ConceptService } from '../providers/concept-service';
import { AppErrorHandler } from '../providers/app-error-handler';

@NgModule({
  declarations: [
    MyApp,
    StartPage,
    RegisterPage,
    ConceptListPage,
    MainConceptDetailsPage,
    MainConceptEditPage,
    MainConceptNewPage,
    SortOptionsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  entryComponents: [
    MyApp,
    StartPage,
    RegisterPage,
    ConceptListPage,
    MainConceptDetailsPage,
    MainConceptEditPage,
    MainConceptNewPage,
    SortOptionsPage,
  ],
  providers: [
    {provide: ErrorHandler, useClass: AppErrorHandler},
    AuthenticationService,
    ConceptService,
    Storage
  ],
  bootstrap: [
    IonicApp
  ]
})

export class AppModule {
    private errorHandler: AppErrorHandler;
    constructor () {
        this.errorHandler = new AppErrorHandler();
    }
}
