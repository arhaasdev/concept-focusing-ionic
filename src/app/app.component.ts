import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { AuthenticationService } from '../providers/authentication-service';
import { StartPage } from '../pages/start/start';
import { ConceptListPage } from '../pages/concept-list/concept-list';
import { MainConceptNewPage } from '../pages/main-concept-new/main-concept-new';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make StartPage the root (or first) page
  rootPage: any = StartPage;

  constructor(
      public platform: Platform,
      public menu: MenuController,
      private authenticationService: AuthenticationService) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  // navigate to the concept list page
   openConceptListPage() {
     this.authenticationService.logout()
     .then(() => {
       this.openPage(ConceptListPage);
     })
 }

 // navigate to the main concept new page
  openMainConceptNewPage() {
    this.openPage(MainConceptNewPage);
}

// logout and return to start page
logout() {
    this.openPage(StartPage);
}

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page);
  }
}
